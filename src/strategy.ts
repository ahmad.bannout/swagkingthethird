import { handleEventsForPlanet } from "./planet/application/index.js"
import { handleEventsForRobot } from "./robot/application/index.js"
import { handleEventsForBank } from "./trading/application/index.js"
import * as relay from "./common/net/relay.js"
import logger from "./utils/logger.js"
import bankService from "./trading/usecase/index.js"
import { getCurrentRoundNumber } from "./state/game.js"
import { buyRobots } from "./trading/application/gateways/commands/buy-robot.js"
import robotService, {
  canMineResourceForRobot,
  getAllRobots,
  canmMineResourceForRobotList,
} from "./robot/usecase/index.js"
import { mineResources } from "./robot/application/gateways/command/mine-robot.js"
import { buyUpdates } from "./trading/application/gateways/commands/upgrade-robot.js"
import { sellResource } from "./trading/application/gateways/commands/sell-resources.js"
import Robot from "./robot/entity/robot.js"
import planetService from "./planet/usecase/index.js"
import { moveRobot } from "./robot/application/gateways/command/move-robot.js"
import { listenerCount } from "process"

export default function strategy() {
  handleEventsForBank()
  handleEventsForRobot()
  handleEventsForPlanet()
  theHundredRoundStrategy()
}

function theHundredRoundStrategy() {
  relay.on("RoundStatus", async (event, context) => {
    const { payload } = event

    if (payload.roundStatus === "started") {
      if (payload.roundNumber < 4) {
        buyRobots(5)
      }
      const robotList = await getAllRobots()
      if (payload.roundNumber > 3) {
        buyRobotsStrategy()
        //updateMiningLevelForRobots()
        manageRobots(robotList)
      }
    }
  })
}
export async function buyRobotsStrategy() {
  const bankBalance = await bankService.getBank()
  logger.info(
    `bank balance is ${bankBalance}  for Round number ${getCurrentRoundNumber()}`
  )

  const canBuyRobots = Math.floor(bankBalance / 100) - 2
  logger.info(
    `trying to buy ${canBuyRobots} Robots for Round number ${getCurrentRoundNumber()}`
  )
  await buyRobots(canBuyRobots)
}

//toDo resource anpassen an level von Robot

async function updateMiningLevelForRobots() {
  const robot = await robotService.getAllRobots()
  const bankBalance = await bankService.getBank()
  const robotToUpgrade = robot.filter((robot) => {
    return robot.miningSpeed == 0
  })
  if (bankBalance >= robotToUpgrade.length * 50 && robotToUpgrade.length > 0) {
    logger.info(
      "trying to upgrade robot with id: " +
        robot[0].id +
        " in planet: " +
        robot[0].planet.planetId
    )
    robotToUpgrade.forEach(async (robot) => {
      await buyUpdates(robot.id, "MINING_SPEED_1", robot.planet.planetId)
    })
  }
}

async function manageRobots(robotList: Robot[]): Promise<Robot[]> {
  // Separate robots that can mine from those that can't
  const robotsAbleToMine = getRobotsAbleToMine(robotList)

  const robotsToMoveOrSell = getRobotsToMoveOrSell(robotList)

  // Robots mining
  for (const robot of await robotsAbleToMine) {
    logger.info(
      `Robot ${robot.id} with lvl ${robot.robotLevels.miningLevel} mining ${robot.planet.resourceType} on planet ${robot.planet.planetId}`
    )
    await mineResources(robot.id)
  }
  // Robots that need to move or sell resources
  for (const robot of await robotsToMoveOrSell) {
    if (robot.inventory.full) {
      logger.info(`Selling resources for robot: ${robot.id}`)
      await sellResource(robot.id)
    } else {
      const planetToMoveTo = await planetService.getPlanetToMoveTo(robot)
      logger.info(
        `Moving robot with id: ${robot.id} to planet: ${planetToMoveTo}`
      )
      await moveRobot(robot.id, planetToMoveTo)
    }
  }

  return robotList
}

const getRobotsAbleToMine = async (robotList: Robot[]): Promise<Robot[]> => {
  const robotsAbleToMine: Robot[] = []

  for (const robot of robotList) {
    if (robot.inventory.full) continue

    const canMine = await canMineResourceForRobot(robot)
    if (canMine) {
      robotsAbleToMine.push(robot)
    }
  }

  return robotsAbleToMine
}
const getRobotsToMoveOrSell = async (robotList: Robot[]): Promise<Robot[]> => {
  const robotsNotAbleToMine: Robot[] = []

  for (const robot of robotList) {
    if (robot.inventory.full) continue

    const canNotMine = await canMineResourceForRobot(robot)
    if (!canNotMine) {
      robotsNotAbleToMine.push(robot)
    }
  }
  return robotsNotAbleToMine
}
