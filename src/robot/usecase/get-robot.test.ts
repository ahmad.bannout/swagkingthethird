import { jest } from "@jest/globals"
import makegetRobot, { makeGetAllRobots } from "./get-robot"
import robotDataSource from "../data-access/robot-db"

// Define mock robot outside the describe blocks so it's accessible in both tests
const mockRobot = {
  id: "1",
  alive: true,
  player: "SwagKingOnTopALWAYS!!",
  planet: { planetId: "1", resourceType: "COAL" },
  maxHealth: 1000,
  maxEnergy: 10,
  energyRegen: 10,
  attackDamage: 10,
  miningSpeed: 10,
  health: 10,
  energy: 10,
  inventory: {
    storageLevel: 0,
    resources: {
      COAL: 0,
      IRON: 0,
      GEM: 0,
      GOLD: 0,
      PLATIN: 0,
    },
    maxStorage: 0,
    usedStorage: 0,
    full: false,
  },
  robotLevels: {
    healthLevel: 0,
    damageLevel: 0,
    miningSpeedLevel: 0,
    miningLevel: 0,
    energyLevel: 0,
    energyRegenLevel: 0,
    storageLevel: 0,
  },
}

describe("makegetRobot", () => {
  let robotRepo: ReturnType<typeof robotDataSource>
  let getRobot: ReturnType<typeof makegetRobot>

  const mockRobotId = "1"

  beforeEach(() => {
    robotRepo = robotDataSource()
    getRobot = makegetRobot({ robotRepo })

    jest.spyOn(robotRepo, "getRobot").mockResolvedValue(mockRobot)
  })

  it("should retrieve a robot by ID", async () => {
    const result = await getRobot(mockRobotId)

    expect(robotRepo.getRobot).toHaveBeenCalledTimes(1)
    expect(robotRepo.getRobot).toHaveBeenCalledWith(mockRobotId)
    expect(result).toEqual(mockRobot)
  })

  it("should throw an error if robot is not found", async () => {
    jest.spyOn(robotRepo, "getRobot").mockResolvedValue(undefined) // Simulate robot not found

    await expect(getRobot(mockRobotId)).rejects.toThrow(
      `Failed to retrieve robot`
    )
  })

  it("should throw a generic error if getRobot fails", async () => {
    jest
      .spyOn(robotRepo, "getRobot")
      .mockRejectedValue(new Error("Failed to retrieve"))

    await expect(getRobot(mockRobotId)).rejects.toThrow(
      "Failed to retrieve robot"
    )
  })
})

describe("makeGetAllRobots", () => {
  let robotRepo: ReturnType<typeof robotDataSource>
  let getAllRobots: ReturnType<typeof makeGetAllRobots>

  const mockRobotList = [mockRobot]

  beforeEach(() => {
    robotRepo = robotDataSource()
    getAllRobots = makeGetAllRobots({ robotRepo })

    jest.spyOn(robotRepo, "getAllRobots").mockResolvedValue(mockRobotList)
  })

  it("should retrieve all robots", async () => {
    const result = await getAllRobots()

    expect(robotRepo.getAllRobots).toHaveBeenCalledTimes(1)
    expect(result).toEqual(mockRobotList)
  })

  it("should throw a generic error if getAllRobots fails", async () => {
    jest
      .spyOn(robotRepo, "getAllRobots")
      .mockRejectedValue(new Error("Failed to retrieve all"))

    await expect(getAllRobots()).rejects.toThrow(
      "Failed to retrieve all robots"
    )
  })
})
