import { jest } from "@jest/globals"
import { makeUpdateInventory } from "./mine-resource"
import robotDataSource from "../data-access/robot-db"
import { RobotResourceMined } from "../../common/types"
import Robot from "../entity/robot"

const mockRobot: Robot = {
  id: "robot-123",
  alive: true,
  player: "SwagKingOnTopALWAYS!!",
  planet: { planetId: "planet-xyz", resourceType: "COAL" },
  maxHealth: 1000,
  maxEnergy: 10,
  energyRegen: 10,
  attackDamage: 10,
  miningSpeed: 10,
  health: 10,
  energy: 10,
  inventory: {
    storageLevel: 0,
    resources: {
      COAL: 0,
      IRON: 0,
      GEM: 0,
      GOLD: 0,
      PLATIN: 0,
    },
    maxStorage: 100,
    usedStorage: 0,
    full: false,
  },
  robotLevels: {
    healthLevel: 0,
    damageLevel: 0,
    miningSpeedLevel: 0,
    miningLevel: 0,
    energyLevel: 0,
    energyRegenLevel: 0,
    storageLevel: 0,
  },
}

const mockResourceMined: RobotResourceMined = {
  robotId: "robot-123",
  minedAmount: 20,
  resourceInventory: {
    COAL: 20,
    IRON: 0,
    GEM: 0,
    GOLD: 0,
    PLATIN: 0,
  },
  minedResource: undefined,
}

describe("makeUpdateInventory", () => {
  let robotRepo: ReturnType<typeof robotDataSource>
  let updateInventory: ReturnType<typeof makeUpdateInventory>

  beforeEach(() => {
    robotRepo = robotDataSource()
    updateInventory = makeUpdateInventory({ robotRepo }) // Initialize after robotRepo

    jest.spyOn(robotRepo, "getAllRobots").mockResolvedValue([mockRobot])
    jest.spyOn(robotRepo, "updateRobot").mockResolvedValue()
  })

  it("should update the robot's inventory", async () => {
    await updateInventory(mockResourceMined)

    expect(robotRepo.getAllRobots).toHaveBeenCalledTimes(1)
    expect(robotRepo.updateRobot).toHaveBeenCalledTimes(1)

    expect(robotRepo.updateRobot).toHaveBeenCalledWith(
      expect.objectContaining({
        id: "robot-123",
        inventory: expect.objectContaining({
          usedStorage: 20,
          resources: {
            COAL: 20,
            IRON: 0,
            GEM: 0,
            GOLD: 0,
            PLATIN: 0,
          },
          full: false,
        }),
      })
    )
  })

  it("should handle empty robot list without error", async () => {
    jest.spyOn(robotRepo, "getAllRobots").mockResolvedValue([])

    await expect(updateInventory(mockResourceMined)).resolves.toBeUndefined()
  })

  it("should throw a generic error if update fails", async () => {
    jest
      .spyOn(robotRepo, "updateRobot")
      .mockRejectedValue(new Error("Update failed"))

    await expect(updateInventory(mockResourceMined)).rejects.toThrow(
      "Failed to update inventory"
    )
  })
})
