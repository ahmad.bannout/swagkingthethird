import { jest } from "@jest/globals"
import robotService from "./index"
import robotDataSource from "../data-access/robot-db"
import Robot from "../entity/robot"
import makeRegenerateRobot from "./regenrate-energy"

// Mock robot data
const mockRobot: Robot = {
  id: "robot-123",
  alive: true,
  player: "SwagKingOnTopALWAYS!!",
  planet: { planetId: "planet-xyz", resourceType: "COAL" },
  maxHealth: 1000,
  maxEnergy: 100,
  energyRegen: 10,
  attackDamage: 10,
  miningSpeed: 10,
  health: 1000,
  energy: 10, // Initial energy value
  inventory: {
    storageLevel: 0,
    resources: {
      COAL: 0,
      IRON: 0,
      GEM: 0,
      GOLD: 0,
      PLATIN: 0,
    },
    maxStorage: 100,
    usedStorage: 0,
    full: false,
  },
  robotLevels: {
    healthLevel: 0,
    damageLevel: 0,
    miningSpeedLevel: 0,
    miningLevel: 0,
    energyLevel: 0,
    energyRegenLevel: 0,
    storageLevel: 0,
  },
}

jest.mock("./index", () => ({
  getRobot: jest.fn(),
}))

describe("makeRegenerateRobot", () => {
  let robotRepo: ReturnType<typeof robotDataSource>
  let regenerateRobot: ReturnType<typeof makeRegenerateRobot>

  const mockRobotId = "robot-123"
  const availableEnergy = 50 // New energy to set for the robot

  beforeEach(() => {
    robotRepo = robotDataSource()
    regenerateRobot = makeRegenerateRobot({ robotRepo })

    // Mock the getRobot and saveRobot methods
    jest.spyOn(robotService, "getRobot").mockResolvedValue(mockRobot)
    jest.spyOn(robotRepo, "saveRobot").mockResolvedValue()
  })

  it("should successfully regenerate the robot's energy", async () => {
    await regenerateRobot(mockRobotId, availableEnergy)

    // Check that the robot's energy is updated
    expect(robotService.getRobot).toHaveBeenCalledTimes(1)
    expect(robotService.getRobot).toHaveBeenCalledWith(mockRobotId)

    expect(robotRepo.saveRobot).toHaveBeenCalledTimes(1)
    expect(robotRepo.saveRobot).toHaveBeenCalledWith(
      expect.objectContaining({
        id: mockRobotId,
        energy: availableEnergy, // The robot's energy should be updated
      })
    )
  })

  it("should throw an error if the robot is not found", async () => {
    jest.spyOn(robotService, "getRobot").mockResolvedValue(undefined)

    await expect(regenerateRobot(mockRobotId, availableEnergy)).rejects.toThrow(
      "Failed to regenerate energy"
    )
  })

  it("should throw a generic error if saving the robot fails", async () => {
    jest
      .spyOn(robotRepo, "saveRobot")
      .mockRejectedValue(new Error("Save failed"))

    await expect(regenerateRobot(mockRobotId, availableEnergy)).rejects.toThrow(
      "Failed to regenerate energy"
    )
  })
})
