import { RobotDependencies } from "../../common/dependencies/robot-dependency"

export default function makegetRobot({ robotRepo }: RobotDependencies) {
  return async (robotId: string) => {
    try {
      if (robotId) {
        const robot = await robotRepo.getRobot(robotId)
        if (!robot) {
          throw new Error(`Robot with ID ${robotId} not found`)
        }
        return robot
      }
    } catch (error) {
      console.error(`Error retrieving robot: ${(error as Error).message}`)
      throw new Error("Failed to retrieve robot")
    }
  }
}

export function makeGetAllRobots({ robotRepo }: RobotDependencies) {
  return async () => {
    try {
      return await robotRepo.getAllRobots()
    } catch (error) {
      console.error(`Error retrieving all robots: ${(error as Error).message}`)
      throw new Error("Failed to retrieve all robots")
    }
  }
}
