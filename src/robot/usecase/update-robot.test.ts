import { jest } from "@jest/globals"
import makeUpdateRobot from "./update-robot"
import Robot from "../entity/robot"
import RobotRepository from "../repo/robotRepo"

// Mock robot data
const mockRobot: Robot = {
  id: "robot-123",
  alive: true,
  player: "SwagKingOnTopALWAYS!!",
  planet: { planetId: "planet-xyz", resourceType: "COAL" },
  maxHealth: 1000,
  maxEnergy: 100,
  energyRegen: 10,
  attackDamage: 10,
  miningSpeed: 10,
  health: 1000,
  energy: 50, // Initial energy value
  inventory: {
    storageLevel: 0,
    resources: {
      COAL: 50,
      IRON: 0,
      GEM: 0,
      GOLD: 0,
      PLATIN: 0,
    },
    maxStorage: 100,
    usedStorage: 50,
    full: true,
  },
  robotLevels: {
    healthLevel: 0,
    damageLevel: 0,
    miningSpeedLevel: 0,
    miningLevel: 0,
    energyLevel: 0,
    energyRegenLevel: 0,
    storageLevel: 0,
  },
}

jest.mock("../repo/robotRepo", () => ({
  getRobot: jest.fn(),
  updateRobot: jest.fn(),
}))

describe("makeUpdateRobot", () => {
  let updateRobot: ReturnType<typeof makeUpdateRobot>
  let robotRepo: jest.Mocked<RobotRepository>

  beforeEach(() => {
    robotRepo = {
      getRobot: jest.fn(),
      updateRobot: jest.fn(),
    } as unknown as jest.Mocked<RobotRepository>

    updateRobot = makeUpdateRobot({ robotRepo })

    jest.spyOn(robotRepo, "getRobot").mockResolvedValue(mockRobot)
    jest.spyOn(robotRepo, "updateRobot").mockResolvedValue()
  })

  it("should successfully update the robot", async () => {
    await updateRobot(mockRobot)

    // Ensure the robot is fetched and updated
    expect(robotRepo.getRobot).toHaveBeenCalledWith(mockRobot.id)
    expect(robotRepo.updateRobot).toHaveBeenCalledWith(mockRobot)
  })

  it("should throw an error if the robot is not found", async () => {
    jest.spyOn(robotRepo, "getRobot").mockResolvedValue(undefined) // Simulate no robot found

    await expect(updateRobot(mockRobot)).rejects.toThrow(
      `Failed to update robot`
    )
  })

  it("should throw an error if the robot ID is changed", async () => {
    const robotWithDifferentId = { ...mockRobot, id: "new-id" }

    await expect(updateRobot(robotWithDifferentId)).rejects.toThrow(
      "Failed to update robot"
    )
  })

  it("should throw a generic error if updating the robot fails", async () => {
    jest
      .spyOn(robotRepo, "updateRobot")
      .mockRejectedValue(new Error("Update failed"))

    await expect(updateRobot(mockRobot)).rejects.toThrow(
      "Failed to update robot"
    )
  })
})
