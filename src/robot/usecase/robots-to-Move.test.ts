import { jest } from "@jest/globals"
import planetService from "../../planet/usecase/index.js"
import Robot from "../entity/robot.js"
import makeRobotsToMove from "./robots-to-Move.js"
import Planet from "../../planet/entity/planet.js"

const mockRobot: Robot = {
  id: "robot-123",
  alive: true,
  player: "SwagKingOnTopALWAYS!!",
  planet: { planetId: "planet-xyz", resourceType: "COAL" },
  maxHealth: 1000,
  maxEnergy: 100,
  energyRegen: 10,
  attackDamage: 10,
  miningSpeed: 10,
  health: 1000,
  energy: 50,
  inventory: {
    storageLevel: 0,
    resources: {
      COAL: 0,
      IRON: 0,
      GEM: 0,
      GOLD: 0,
      PLATIN: 0,
    },
    maxStorage: 100,
    usedStorage: 0,
    full: false,
  },
  robotLevels: {
    healthLevel: 0,
    damageLevel: 0,
    miningSpeedLevel: 0,
    miningLevel: 0,
    energyLevel: 0,
    energyRegenLevel: 0,
    storageLevel: 0,
  },
}

const mockPlanet: Planet = {
  planet: "planet-xyz",
  movementDifficulty: 0,
  neighbours: [],
  resource: undefined,
}

jest.mock("../../planet/usecase/index.js", () => ({
  getPlanet: jest.fn(),
}))

describe("makeRobotsToMove", () => {
  let robotsToMove: ReturnType<typeof makeRobotsToMove>

  beforeEach(() => {
    robotsToMove = makeRobotsToMove()

    jest.spyOn(planetService, "getPlanet").mockResolvedValue(mockPlanet)
  })

  it("should return robots that have enough energy and are not on a COAL planet", async () => {
    const robotList = [mockRobot]

    const result = await robotsToMove(robotList)

    expect(planetService.getPlanet).toHaveBeenCalledTimes(1)
    expect(planetService.getPlanet).toHaveBeenCalledWith(
      mockRobot.planet.planetId
    )

    expect(result).toEqual([mockRobot])
  })

  it("should return an empty list if no robots can move", async () => {
    const modifiedRobot = { ...mockRobot, energy: 10 }
    jest.spyOn(planetService, "getPlanet").mockResolvedValue({
      ...mockPlanet,
      movementDifficulty: 20,
      planet: "",
      neighbours: [],
    })

    const result = await robotsToMove([modifiedRobot])

    expect(result).toEqual([])
  })

  it("should skip robots on a COAL planet", async () => {
    jest.spyOn(planetService, "getPlanet").mockResolvedValue({
      ...mockPlanet,
      resource: { currentAmount: 10, maxAmount: 100, type: "COAL" },
    })

    const result = await robotsToMove([mockRobot])

    expect(result).toEqual([])
  })

  it("should handle an empty robot list", async () => {
    const result = await robotsToMove([])

    expect(result).toEqual([])
  })

  it("should throw an error if something goes wrong", async () => {
    jest
      .spyOn(planetService, "getPlanet")
      .mockRejectedValue(new Error("Service failure"))

    await expect(robotsToMove([mockRobot])).rejects.toThrow(
      "Failed to determine robots to move"
    )
  })
})
