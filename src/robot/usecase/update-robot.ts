import Robot from "../entity/robot"
import RobotRepository from "../repo/robotRepo"

interface Dependencies {
  robotRepo: RobotRepository
}
export default function makeUpdateRobot({ robotRepo }: Dependencies) {
  return async (robot: Robot) => {
    try {
      const existingRobot = await robotRepo.getRobot(robot.id)
      if (!existingRobot) {
        throw new Error(`Robot with ID ${robot.id} not found`)
      }

      if (robot.id !== existingRobot.id) {
        throw new Error("Cannot change robot ID")
      }

      await robotRepo.updateRobot(robot)
    } catch (error) {
      console.error(`Error updating robot: ${(error as Error).message}`)
      throw new Error("Failed to update robot")
    }
  }
}
