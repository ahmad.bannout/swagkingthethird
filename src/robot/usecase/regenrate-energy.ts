import { RobotDependencies } from "../../common/dependencies/robot-dependency.js"
import robotService from "./index.js"

export default function makeRegenerateRobot({ robotRepo }: RobotDependencies) {
  return async (robotId: string, availableEnergy: number) => {
    try {
      const robot = await robotService.getRobot(robotId)
      if (!robot) throw new Error("Robot has not been found")
      robot.energy = availableEnergy
      await robotRepo.saveRobot(robot)
    } catch (error) {
      console.error(`Error regenerating energy: ${(error as Error).message}`)
      throw new Error("Failed to regenerate energy")
    }
  }
}
