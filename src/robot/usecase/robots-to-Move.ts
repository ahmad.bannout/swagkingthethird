import planetService from "../../planet/usecase/index.js"
import Robot from "../entity/robot.js"

export default function makeRobotsToMove() {
  return async (robotList: Robot[]): Promise<Robot[]> => {
    try {
      if (robotList.length === 0) {
        return []
      }
      const robotsToMove: Robot[] = []
      for (const robot of robotList) {
        const planet = await planetService.getPlanet(robot.planet.planetId)
        if (!planet) {
          continue // Skip if planet not found
        }

        const canMove =
          robot.energy >= planet.movementDifficulty &&
          planet.resource?.type !== "COAL"
        if (canMove) {
          robotsToMove.push(robot)
        }
      }

      return robotsToMove
    } catch (error) {
      console.error(
        `Error determining robots to move: ${(error as Error).message}`
      )
      throw new Error("Failed to determine robots to move")
    }
  }
}
