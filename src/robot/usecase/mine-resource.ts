import { RobotDependencies } from "../../common/dependencies/robot-dependency"
import { RobotResourceMined, RobotResourceRemoved } from "../../common/types"
import logger from "../../utils/logger.js"
import Robot from "../entity/robot"

export function makeMineResourceList() {
  return async (mininglist: Robot[]): Promise<Robot[]> => {
    if (mininglist.length === 0) {
      return []
    }

    const filteredMiningList = mininglist.filter((robot) => {
      return canMine(robot)
    })

    return filteredMiningList
  }
}
export function makeMineResourceForRobot() {
  return async (robot: Robot): Promise<boolean> => {
    if (!robot) {
      throw new Error(`cant mine with robot, because robot is undefined!!`)
    }

    return canMine(robot)
  }
}

export function makeUpdateInventory({ robotRepo }: RobotDependencies) {
  return async (
    resourceMined: RobotResourceMined | RobotResourceRemoved
  ): Promise<void> => {
    try {
      const list = await robotRepo.getAllRobots()
      if (list.length == 0) {
        return
      }
      const robot = list.find((robot) => robot.id == resourceMined.robotId)
      if (!robot) {
        throw new Error(
          `cant update Inventory of Robot with id: ${resourceMined.robotId}, because robot does not exist in robot list!!`
        )
      }
      const newRobot = updateRobotInventory(robot, resourceMined)
      //test
      await robotRepo.updateRobot(newRobot)
    } catch (error) {
      console.error(`Error updateing inventory: ${(error as Error).message}`)
      throw new Error("Failed to update inventory")
    }
  }
}

const updateRobotInventory = (
  robot: Robot,
  resourceEvent: RobotResourceMined | RobotResourceRemoved
): Robot => {
  if ("minedAmount" in resourceEvent) {
    robot.inventory.usedStorage += resourceEvent.minedAmount
  } else if ("removedAmount" in resourceEvent) {
    robot.inventory.usedStorage -= resourceEvent.removedAmount
  }
  const maxInventory = robot.inventory.maxStorage
  robot.inventory.full = robot.inventory.usedStorage === maxInventory
  robot.inventory.resources = resourceEvent.resourceInventory
  return robot
}
enum ResourceType {
  COAL = "COAL",
  IRON = "IRON",
  GEM = "GEM",
  GOLD = "GOLD",
  PLATINUM = "PLATINUM",
}
const resourceMiningLevels: { [key in ResourceType]: number } = {
  [ResourceType.COAL]: 0,
  [ResourceType.IRON]: 1,
  [ResourceType.GEM]: 2,
  [ResourceType.GOLD]: 3,
  [ResourceType.PLATINUM]: 4,
}

const canMine = (robot: Robot): boolean => {
  const resourceType = robot.planet.resourceType
  const miningLevel = robot.robotLevels.miningLevel

  if (!resourceType || robot.inventory.full || !robot.planet.resourceType) {
    return false
  }

  const requiredLevel = resourceMiningLevels[resourceType as ResourceType]
  logger.warn(
    `req lvl: ${requiredLevel} and mininglvl ${miningLevel} the ergebnis ${
      miningLevel === requiredLevel
    }`
  )

  return miningLevel === requiredLevel
}
