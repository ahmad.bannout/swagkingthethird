import { jest } from "@jest/globals"
import robotService from "./index.js"
import Robot from "../entity/robot.js"
import makeSellResources from "./sell-resources.js"

const mockRobotFullStorage: Robot = {
  id: "robot-123",
  alive: true,
  player: "SwagKingOnTopALWAYS!!",
  planet: { planetId: "planet-xyz", resourceType: "COAL" },
  maxHealth: 1000,
  maxEnergy: 100,
  energyRegen: 10,
  attackDamage: 10,
  miningSpeed: 10,
  health: 1000,
  energy: 10,
  inventory: {
    storageLevel: 1,
    resources: {
      COAL: 50,
      IRON: 0,
      GEM: 0,
      GOLD: 0,
      PLATIN: 0,
    },
    maxStorage: 50,
    usedStorage: 50,
    full: true,
  },
  robotLevels: {
    healthLevel: 0,
    damageLevel: 0,
    miningSpeedLevel: 0,
    miningLevel: 0,
    energyLevel: 0,
    energyRegenLevel: 0,
    storageLevel: 0,
  },
}

const mockRobotNotFullStorage: Robot = {
  ...mockRobotFullStorage,
  id: "robot-456",
  inventory: {
    ...mockRobotFullStorage.inventory,
    usedStorage: 20,
    full: false,
  },
}

jest.mock("./index.js", () => ({
  getAllRobots: jest.fn(),
}))

describe("makeSellResources", () => {
  let sellResources: ReturnType<typeof makeSellResources>

  beforeEach(() => {
    sellResources = makeSellResources()

    jest
      .spyOn(robotService, "getAllRobots")
      .mockResolvedValue([mockRobotFullStorage, mockRobotNotFullStorage])
  })

  it("should return robots that have full inventory", async () => {
    const result = await sellResources()

    expect(robotService.getAllRobots).toHaveBeenCalledTimes(1)

    expect(result).toEqual([mockRobotFullStorage])
  })

  it("should return an empty list if no robots have full inventory", async () => {
    jest
      .spyOn(robotService, "getAllRobots")
      .mockResolvedValue([mockRobotNotFullStorage])

    const result = await sellResources()

    expect(result).toEqual([])
  })

  it("should return an empty list if no robots are found", async () => {
    jest.spyOn(robotService, "getAllRobots").mockResolvedValue([])

    const result = await sellResources()

    expect(result).toEqual([])
  })

  it("should throw an error if something goes wrong", async () => {
    jest
      .spyOn(robotService, "getAllRobots")
      .mockRejectedValue(new Error("Service failure"))

    await expect(sellResources()).rejects.toThrow("Failed to sell resources")
  })
})
