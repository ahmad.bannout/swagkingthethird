import Robot from "../entity/robot.js"
import robotService from "./index.js"

export default function makeSellResources() {
  return async (): Promise<Robot[]> => {
    try {
      const robots = await robotService.getAllRobots()
      if (!robots.length) {
        return []
      }
      const robotWithMaxStorage: Robot[] = robots.filter(
        (robot: Robot) => robot.inventory.full
      )
      return robotWithMaxStorage
    } catch (error) {
      console.error(`Error selling resources: ${(error as Error).message}`)
      throw new Error("Failed to sell resources")
    }
  }
}
