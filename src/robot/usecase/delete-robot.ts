import { RobotDependencies } from "../../common/dependencies/robot-dependency"

export default function makeDeleteRobot({ robotRepo }: RobotDependencies) {
  return async (robotId: string): Promise<void> => {
    try {
      const robot = await robotRepo.getRobot(robotId)
      if (!robot) {
        throw new Error(`Robot with ID ${robotId} not found`)
      }
      await robotRepo.deleteRobot(robotId)
    } catch (error) {
      console.error(`Error deleting robot: ${(error as Error).message}`)
      throw new Error("Failed to delete robot")
    }
  }
}
