import { jest } from "@jest/globals"
import makeAddRobot from "./add-robot"
import Robot from "../entity/robot"
import planetDataSource from "../../planet/data-access/planet-db"
import robotDataSource from "../data-access/robot-db"

const mockRobot: Robot = {
  id: "1",
  alive: true,
  player: "SwagKingOnTopALWAYS!!",
  planet: { planetId: "1", resourceType: "COAL" },
  maxHealth: 1000,
  maxEnergy: 10,
  energyRegen: 10,
  attackDamage: 10,
  miningSpeed: 10,
  health: 10,
  energy: 10,
  inventory: {
    storageLevel: 0,
    resources: {
      COAL: 0,
      IRON: 0,
      GEM: 0,
      GOLD: 0,
      PLATIN: 0,
    },
    maxStorage: 0,
    usedStorage: 0,
    full: false,
  },
  robotLevels: {
    healthLevel: 0,
    damageLevel: 0,
    miningSpeedLevel: 0,
    miningLevel: 0,
    energyLevel: 0,
    energyRegenLevel: 0,
    storageLevel: 0,
  },
}

describe("makeAddRobot", () => {
  let planetRepo: ReturnType<typeof planetDataSource>
  let robotRepo: ReturnType<typeof robotDataSource>
  let addRobot: ReturnType<typeof makeAddRobot>

  beforeEach(() => {
    planetRepo = planetDataSource()
    robotRepo = robotDataSource()
    addRobot = makeAddRobot({ robotRepo }, { planetRepo })

    jest.spyOn(planetRepo, "savePlanet")
    jest.spyOn(robotRepo, "saveRobot")
  })

  it("should call savePlanetForRobot and saveRobot with the provided robot", async () => {
    await addRobot(mockRobot)

    expect(robotRepo.saveRobot).toHaveBeenCalledTimes(1)
    expect(robotRepo.saveRobot).toHaveBeenCalledWith(mockRobot)

    const savedRobot = await robotRepo.getRobot(mockRobot.id)
    expect(savedRobot).toEqual(mockRobot)
  })
})
