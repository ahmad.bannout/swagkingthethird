import { jest } from "@jest/globals"
import makeDeleteRobot from "./delete-robot"
import robotDataSource from "../data-access/robot-db"

describe("makeDeleteRobot", () => {
  let robotRepo: ReturnType<typeof robotDataSource>
  let deleteRobot: ReturnType<typeof makeDeleteRobot>

  const mockRobotId = "1"
  const mockRobot = {
    id: "1",
    alive: true,
    player: "SwagKingOnTopALWAYS!!",
    planet: { planetId: "1", resourceType: "COAL" },
    maxHealth: 1000,
    maxEnergy: 10,
    energyRegen: 10,
    attackDamage: 10,
    miningSpeed: 10,
    health: 10,
    energy: 10,
    inventory: {
      storageLevel: 0,
      resources: {
        COAL: 0,
        IRON: 0,
        GEM: 0,
        GOLD: 0,
        PLATIN: 0,
      },
      maxStorage: 0,
      usedStorage: 0,
      full: false,
    },
    robotLevels: {
      healthLevel: 0,
      damageLevel: 0,
      miningSpeedLevel: 0,
      miningLevel: 0,
      energyLevel: 0,
      energyRegenLevel: 0,
      storageLevel: 0,
    },
  }

  beforeEach(() => {
    robotRepo = robotDataSource()
    deleteRobot = makeDeleteRobot({ robotRepo })

    jest.spyOn(robotRepo, "getRobot").mockResolvedValue(mockRobot)
    jest.spyOn(robotRepo, "deleteRobot").mockResolvedValue()
  })

  it("should call deleteRobot with the correct robot ID", async () => {
    await deleteRobot(mockRobotId)

    expect(robotRepo.getRobot).toHaveBeenCalledTimes(1)
    expect(robotRepo.getRobot).toHaveBeenCalledWith(mockRobotId)

    expect(robotRepo.deleteRobot).toHaveBeenCalledTimes(1)
    expect(robotRepo.deleteRobot).toHaveBeenCalledWith(mockRobotId)
  })

  it("should throw an error if the robot is not found", async () => {
    jest.spyOn(robotRepo, "getRobot").mockResolvedValue(undefined)

    await expect(deleteRobot(mockRobotId)).rejects.toThrow(
      `Failed to delete robot`
    )
  })

  it("should throw a generic error if deleteRobot fails", async () => {
    jest
      .spyOn(robotRepo, "deleteRobot")
      .mockRejectedValue(new Error("Delete failed"))

    await expect(deleteRobot(mockRobotId)).rejects.toThrow(
      "Failed to delete robot"
    )
  })
})
