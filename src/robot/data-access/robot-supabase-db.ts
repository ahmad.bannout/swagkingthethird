import { supabase } from "../../db/supabase-db.js"
import Robot from "../entity/robot.js"
import RobotRepository from "../repo/robotRepo.js"

export default function robotDataSource(): RobotRepository {
  return {
    getRobot: async (id: string) => {
      const { data, error } = await supabase
        .from("robots")
        .select("*")
        .eq("id", id)
        .single()

      if (error) {
        console.error("Error fetching robot:", error)
        return undefined
      }
      return data ? (data as Robot) : undefined
    },
    updateRobot: async (robot: Robot): Promise<void> => {
      const { data, error } = await supabase
        .from("robots")
        .update({
          alive: robot.alive,
          player: robot.player,
          planet_id: robot.planet.planetId,
          resource_type: robot.planet.resourceType,
          max_health: robot.maxHealth,
          health: robot.health,
          max_energy: robot.maxEnergy,
          energy: robot.energy,
          energy_regen: robot.energyRegen,
          attack_damage: robot.attackDamage,
          mining_speed: robot.miningSpeed,
          storage_level: robot.inventory.storageLevel,
          inventory: robot.inventory.resources,
          max_storage: robot.inventory.maxStorage,
          used_storage: robot.inventory.usedStorage,
          full: robot.inventory.full,
          health_level: robot.robotLevels.healthLevel,
          damage_level: robot.robotLevels.damageLevel,
          mining_speed_level: robot.robotLevels.miningSpeedLevel,
          mining_level: robot.robotLevels.miningLevel,
          energy_level: robot.robotLevels.energyLevel,
          energy_regen_level: robot.robotLevels.energyRegenLevel,
        })
        .eq("id", robot.id)

      if (error) {
        throw new Error(
          `Error updating robot with ID ${robot.id}: ${error.message}`
        )
      }
    },
    saveRobot: async (robot: Robot): Promise<void> => {
      const { data, error } = await supabase.from("robots").insert([
        {
          id: robot.id,
          player: robot.player,
          planet_id: robot.planet.planetId,
          resource_type: robot.planet.resourceType,
          alive: robot.alive,
          max_health: robot.maxHealth,
          health: robot.health,
          max_energy: robot.maxEnergy,
          energy: robot.energy,
          energy_regen: robot.energyRegen,
          attack_damage: robot.attackDamage,
          mining_speed: robot.miningSpeed,
          storage_level: robot.inventory.storageLevel,
          inventory: robot.inventory.resources,
          max_storage: robot.inventory.maxStorage,
          used_storage: robot.inventory.usedStorage,
          full: robot.inventory.full,
          health_level: robot.robotLevels.healthLevel,
          damage_level: robot.robotLevels.damageLevel,
          mining_speed_level: robot.robotLevels.miningSpeedLevel,
          mining_level: robot.robotLevels.miningLevel,
          energy_level: robot.robotLevels.energyLevel,
          energy_regen_level: robot.robotLevels.energyRegenLevel,
        },
      ])

      if (error) {
        throw new Error(`Error saving robot: ${error.message}`)
      }
    },
    deleteRobot: async (robotid: string): Promise<void> => {
      const { error } = await supabase.from("robots").delete().eq("id", robotid)

      if (error) {
        throw new Error(
          `Error deleting robot with ID ${robotid}: ${error.message}`
        )
      }
    },
    getAllRobots: async () => {
      const { data, error } = await supabase.from("robots").select("*")

      if (error) {
        throw new Error(`Error fetching all robots: ${error.message}`)
      }

      return data as Robot[]
    },
  }
}
