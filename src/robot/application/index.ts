import { handleAddRobot } from "./interactors/add-robot.js"
import { handleRobotMoved } from "./interactors/move-robot.js"
import { handleUpdateRobotLevel } from "./interactors/update-robot-level.js"
import { handleRobotResourceRemoved } from "./interactors/mine-robot.js"
import { handleUpdateInventory } from "./interactors/update-robot-ressource.js"

const handleAddRobotEvent = handleAddRobot()
const handleUpdateInventoryEvent = handleUpdateInventory()
const handleRobotResourceRemovedEvent = handleRobotResourceRemoved()
const handleRobotMovedEvent = handleRobotMoved()
const handleUpdateRobotLevelEvent = handleUpdateRobotLevel()

export const handleEventsForRobot = () => {
  handleAddRobotEvent()
  handleUpdateInventoryEvent()
  handleRobotResourceRemovedEvent()
  handleRobotMovedEvent()
  handleUpdateRobotLevelEvent()
}
