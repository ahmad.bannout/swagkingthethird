import { sendCommand } from "../../../../common/net/client"
import { mineResources } from "./mine-robot"

jest.mock("../../../../common/net/client")

describe("mineResources", () => {
  const mockRobotId = "robot123"

  beforeEach(() => {
    ;(sendCommand as jest.Mock).mockClear()
  })

  it("should send a mining command with correct robotId", async () => {
    ;(sendCommand as jest.Mock).mockResolvedValue(Promise.resolve())

    await mineResources(mockRobotId)

    expect(sendCommand).toHaveBeenCalledWith({
      type: "mining",
      data: {
        robotId: mockRobotId,
      },
    })
  })

  it("should handle errors when sendCommand rejects", async () => {
    const mockError = new Error("Failed to send command")
    ;(sendCommand as jest.Mock).mockRejectedValue(mockError)

    await expect(mineResources(mockRobotId)).rejects.toThrow(
      "Failed to send command"
    )
  })
})
