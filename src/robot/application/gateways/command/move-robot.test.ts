import { sendCommand } from "../../../../common/net/client"
import { moveRobot } from "./move-robot"

jest.mock("../../../../common/net/client")

describe("moveRobot", () => {
  const mockRobotId = "robot123"
  const mockPlanetId = "planet456"

  beforeEach(() => {
    ;(sendCommand as jest.Mock).mockClear()
  })

  it("should send a movement command with correct robotId and planetId", async () => {
    ;(sendCommand as jest.Mock).mockResolvedValue(Promise.resolve())

    await moveRobot(mockRobotId, mockPlanetId)

    expect(sendCommand).toHaveBeenCalledWith({
      type: "movement",
      data: {
        robotId: mockRobotId,
        planetId: mockPlanetId,
      },
    })
  })

  it("should handle errors when sendCommand rejects", async () => {
    const mockError = new Error("Failed to send command")
    ;(sendCommand as jest.Mock).mockRejectedValue(mockError)

    await expect(moveRobot(mockRobotId, mockPlanetId)).rejects.toThrow(
      "Failed to send command"
    )
  })
})
