import { sendCommand } from "../../../../common/net/client"
import { attackRobot } from "./attack-robot"

jest.mock("../../../../common/net/client")

describe("attackRobot", () => {
  const mockRobotId = "robot123"
  const mockTargetId = "target456"

  beforeEach(() => {
    ;(sendCommand as jest.Mock).mockClear()
  })

  it("should send a battle command with correct robotId and targetId", async () => {
    ;(sendCommand as jest.Mock).mockResolvedValue(Promise.resolve())

    await attackRobot(mockRobotId, mockTargetId)

    expect(sendCommand).toHaveBeenCalledWith({
      type: "battle",
      data: {
        robotId: mockRobotId,
        targetId: mockTargetId,
      },
    })
  })

  it("should handle errors when sendCommand rejects", async () => {
    const mockError = new Error("Failed to send command")
    ;(sendCommand as jest.Mock).mockRejectedValue(mockError)

    // Act & Assert: Expect the function to throw an error
    await expect(attackRobot(mockRobotId, mockTargetId)).rejects.toThrow(
      "Failed to send command"
    )
  })
})
