import * as relay from "../../../common/net/relay"
import { GameEvent, RobotResourceMined } from "../../../common/types"
import logger from "../../../utils/logger"
import { updateRobotInventory } from "../../usecase/index"
import { handleUpdateInventory } from "./update-robot-ressource"

jest.mock("../../../common/net/relay")
jest.mock("../../../utils/logger")
jest.mock("../../usecase/index")

describe("handleUpdateInventory", () => {
  let handleUpdateInventoryInstance: ReturnType<typeof handleUpdateInventory>

  const mockEvent: GameEvent<RobotResourceMined> = {
    headers: {
      type: "RobotResourceMined",
      eventId: "event123",
      timestamp: "2024-09-28T12:34:56Z",
      "kafka-topic": "robot-mining",
    },
    payload: {
      robotId: "robot123",
      minedAmount: 100,
      minedResource: "IRON",
      resourceInventory: {
        IRON: 150,
        COAL: 50,
        GEM: 0,
        GOLD: 0,
        PLATIN: 0,
      },
    },
  }

  const mockContext = {
    playerId: "player123",
    playerExchange: "player-exchange",
  }

  beforeEach(() => {
    // Initialize the function that handles the event
    handleUpdateInventoryInstance = handleUpdateInventory()

    // Mock logger methods
    logger.info = jest.fn()
    logger.error = jest.fn()

    // Mock relay.on to simulate the RobotResourceMined event
    jest.spyOn(relay, "on").mockImplementation((event, callback) => {
      if (event === "RobotResourceMined") {
        callback(mockEvent, mockContext)
      }
    })

    // Mock updateRobotInventory function
    ;(updateRobotInventory as jest.Mock).mockResolvedValue(Promise.resolve())
  })

  it("should log resource mining and call updateRobotInventory with correct data", async () => {
    // Act: Call the handleUpdateInventoryInstance to simulate the event
    await handleUpdateInventoryInstance()

    // Assert: relay.on is set up for "RobotResourceMined"
    expect(relay.on).toHaveBeenCalledWith(
      "RobotResourceMined",
      expect.any(Function)
    )

    // Assert: logger.info logs the mining event
    expect(logger.info).toHaveBeenCalledWith(
      "Robot with id: robot123 has mined 100 of IRON"
    )

    // Assert: updateRobotInventory is called with the correct payload
    expect(updateRobotInventory).toHaveBeenCalledWith(mockEvent.payload)

    // Assert: logger.info logs the updated inventory
    expect(logger.info).toHaveBeenCalledWith(
      `Robot with id: robot123 has mined 100 of IRON`
    )
    expect(logger.info).toHaveBeenCalledWith(
      `The Robot with the Id: robot123 now has this resources in his inventory {"IRON":150,"COAL":50,"GEM":0,"GOLD":0,"PLATIN":0}`
    )
  })

  it("should log errors when updateRobotInventory fails", async () => {
    // Arrange: Mock updateRobotInventory to throw an error
    const mockError = new Error("Inventory update failed")
    ;(updateRobotInventory as jest.Mock).mockRejectedValue(mockError)

    // Act: Call the handleUpdateInventoryInstance to simulate the event
    await handleUpdateInventoryInstance()

    // Assert: logger.info logs the mining event before the error
    expect(logger.info).toHaveBeenCalledWith(
      "Robot with id: robot123 has mined 100 of IRON"
    )

    // Assert: logger.error logs the error message
    expect(logger.error).toHaveBeenCalledWith("Inventory update failed")
    expect(logger.error).toHaveBeenCalledWith(
      "could not update the inventory of a robot!!"
    )
  })
})
