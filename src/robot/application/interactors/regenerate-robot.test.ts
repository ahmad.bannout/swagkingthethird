import logger from "../../../utils/logger"
import * as relay from "../../../common/net/relay"
import robotService from "../../usecase/index"
import { handleRobotRgenerated } from "./regenerate-robot"
import { GameEvent, RobotRegenerated } from "../../../common/types"

jest.mock("../../../utils/logger")
jest.mock("../../usecase/index")

describe("handleRobotRegenerated", () => {
  let handleRobotRegeneratedInstance: ReturnType<typeof handleRobotRgenerated>

  const mockEvent: GameEvent<RobotRegenerated> = {
    headers: {
      type: "RobotRegenerated",
      eventId: "event123",
      timestamp: "2024-09-28T12:34:56Z",
      "kafka-topic": "robot-regeneration",
    },
    payload: {
      robotId: "robot123",
      availableEnergy: 100,
    },
  }

  const mockContext = {
    playerId: "player123",
    playerExchange: "player-exchange",
  }

  beforeEach(() => {
    handleRobotRegeneratedInstance = handleRobotRgenerated()

    logger.info = jest.fn()

    jest.spyOn(relay, "on").mockImplementation((event, callback) => {
      if (event === "RobotRegenerated") {
        callback(mockEvent, mockContext)
      }
    })
    ;(robotService.regenerateRobot as jest.Mock).mockResolvedValue(
      Promise.resolve()
    )
  })

  it("should log robot regeneration and call regenerateRobot with correct data", async () => {
    await handleRobotRegeneratedInstance()

    expect(relay.on).toHaveBeenCalledWith(
      "RobotRegenerated",
      expect.any(Function)
    )

    expect(logger.info).toHaveBeenCalledWith(
      "enrgey generated for" + "robot123" + "Resource for Robot: "
    )

    expect(robotService.regenerateRobot).toHaveBeenCalledWith("robot123", 100)
  })
})
