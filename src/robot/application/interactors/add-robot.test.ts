import logger from "../../../utils/logger"
import * as relay from "../../../common/net/relay"
import robotService from "../../usecase/index"
import Robot, { makeRobot } from "../../entity/robot"
import { handleAddRobot } from "./add-robot"
import { GameEvent, RobotSpawned } from "../../../common/types"

jest.mock("../../../utils/logger")
jest.mock("../../usecase/index")
jest.mock("../../entity/robot")

describe("handleAddRobot", () => {
  let handleAddRobotInstance: ReturnType<typeof handleAddRobot>

  const mockRobot: Robot = {
    id: "1",
    alive: true,
    player: "SwagKingOnTopALWAYS!!",
    planet: { planetId: "1", resourceType: "COAL" },
    maxHealth: 1000,
    maxEnergy: 10,
    energyRegen: 10,
    attackDamage: 10,
    miningSpeed: 10,
    health: 10,
    energy: 10,
    inventory: {
      storageLevel: 0,
      resources: {
        COAL: 0,
        IRON: 0,
        GEM: 0,
        GOLD: 0,
        PLATIN: 0,
      },
      maxStorage: 0,
      usedStorage: 0,
      full: false,
    },
    robotLevels: {
      healthLevel: 0,
      damageLevel: 0,
      miningSpeedLevel: 0,
      miningLevel: 0,
      energyLevel: 0,
      energyRegenLevel: 0,
      storageLevel: 0,
    },
  }

  const mockContext = {
    playerId: "player123",
    playerExchange: "player-exchange",
  }
  const mockEvent: GameEvent<RobotSpawned> = {
    headers: {
      type: "RobotSpawned",
      eventId: "event123",
      timestamp: "2024-09-28T12:34:56Z",
      "kafka-topic": "robot-spawn",
    },
    payload: {
      robot: mockRobot,
    },
  }

  beforeEach(() => {
    handleAddRobotInstance = handleAddRobot()

    logger.info = jest.fn()

    jest.spyOn(relay, "on").mockImplementation((event, callback) => {
      if (event === "RobotSpawned") {
        callback(mockEvent, mockContext)
      }
    })
    ;(robotService.addRobot as jest.Mock).mockReturnValue(1)
    ;(makeRobot as jest.Mock).mockReturnValue({
      id: "1",
      type: "explorer",
      planetId: "Mars",
    })
  })

  it("should log robot spawn and call addRobot with correct data", async () => {
    await handleAddRobotInstance()

    expect(relay.on).toHaveBeenCalledWith("RobotSpawned", expect.any(Function))

    expect(logger.info).toHaveBeenCalledWith(
      "Robot has been spawned with id: 1. you now have 1 Robots in the Map"
    )

    expect(makeRobot).toHaveBeenCalledWith(mockEvent.payload)

    expect(robotService.addRobot).toHaveBeenCalledWith({
      id: "1",
      type: "explorer",
      planetId: "Mars",
    })

    expect(logger.info).toHaveBeenCalledWith(
      "Robot has been spawned with id: 1. you now have 1 Robots in the Map"
    )
  })
})
