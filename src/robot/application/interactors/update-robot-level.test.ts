import * as relay from "../../../common/net/relay"
import { GameEvent, RobotUpgraded } from "../../../common/types"
import logger from "../../../utils/logger"
import Robot from "../../entity/robot"
import robotService from "../../usecase/index"
import { handleUpdateRobotLevel } from "./update-robot-level"

jest.mock("../../../common/net/relay")
jest.mock("../../../utils/logger")
jest.mock("../../usecase/index")

const mockGetRobot = robotService.getRobot as jest.MockedFunction<
  typeof robotService.getRobot
>
const mockUpdateRobot = robotService.updateRobot as jest.MockedFunction<
  typeof robotService.updateRobot
>
const mockRelayOn = relay.on as jest.MockedFunction<typeof relay.on>

describe("handleUpdateRobotLevel", () => {
  let handleUpdateRobotLevelInstance: ReturnType<typeof handleUpdateRobotLevel>

  const mockPlanet = {
    planetId: "planet456",
    resourceType: "IRON",
  }

  const mockRobot: Robot = {
    id: "1",
    alive: true,
    player: "SwagKingOnTopALWAYS!!",
    planet: mockPlanet,
    maxHealth: 1000,
    maxEnergy: 10,
    energyRegen: 10,
    attackDamage: 10,
    miningSpeed: 10,
    health: 10,
    energy: 10,
    inventory: {
      storageLevel: 0,
      resources: {
        COAL: 0,
        IRON: 0,
        GEM: 0,
        GOLD: 0,
        PLATIN: 0,
      },
      maxStorage: 0,
      usedStorage: 0,
      full: false,
    },
    robotLevels: {
      healthLevel: 0,
      damageLevel: 0,
      miningSpeedLevel: 0,
      miningLevel: 0,
      energyLevel: 0,
      energyRegenLevel: 0,
      storageLevel: 0,
    },
  }

  const mockEvent: GameEvent<RobotUpgraded> = {
    headers: {
      type: "RobotUpgraded",
      eventId: "event123",
      timestamp: "2024-09-28T12:34:56Z",
      "kafka-topic": "robot-upgrade",
    },
    payload: {
      robotId: "robot123",
      upgrade: "DAMAGE_1",
      level: 2,
      robot: mockRobot,
    },
  }

  const mockContext = {
    playerId: "player123",
    playerExchange: "player-exchange",
  }

  beforeEach(() => {
    handleUpdateRobotLevelInstance = handleUpdateRobotLevel()

    logger.info = jest.fn()
    logger.error = jest.fn()

    mockRelayOn.mockImplementation((event, callback) => {
      if (event === "RobotUpgraded") {
        callback(mockEvent, mockContext)
      }
    })

    mockGetRobot.mockResolvedValue(mockRobot)
    mockUpdateRobot.mockResolvedValue(Promise.resolve())
  })

  it("should log robot upgrade and call updateRobot with the new level", async () => {
    await handleUpdateRobotLevelInstance()

    expect(mockRelayOn).toHaveBeenCalledWith(
      "RobotUpgraded",
      expect.any(Function)
    )

    expect(logger.info).toHaveBeenCalledWith(
      "Robot with id: robot123 has been updated DAMAGE_1 to level 2"
    )

    expect(mockGetRobot).toHaveBeenCalledWith("robot123")

    expect(mockUpdateRobot).toHaveBeenCalledWith({
      ...mockRobot,
      robotLevels: {
        damageLevel: 2,
        energyLevel: 0,
        energyRegenLevel: 0,
        healthLevel: 0,
        miningLevel: 0,
        miningSpeedLevel: 0,
        storageLevel: 0,
      },
    })
  })
})
