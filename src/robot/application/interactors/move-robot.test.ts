import logger from "../../../utils/logger"
import * as relay from "../../../common/net/relay"
import robotService from "../../usecase/index"
import planetService from "../../../planet/usecase/index"
import { handleRobotMoved } from "./move-robot"
import { GameEvent, Robot, RobotMoved } from "../../../common/types"

jest.mock("../../../utils/logger")
jest.mock("../../usecase/index")
jest.mock("../../../planet/usecase/index")

describe("handleRobotMoved", () => {
  let handleRobotMovedInstance: ReturnType<typeof handleRobotMoved>

  const mockEvent: GameEvent<RobotMoved> = {
    headers: {
      type: "RobotMoved",
      eventId: "event123",
      timestamp: "2024-09-28T12:34:56Z",
      "kafka-topic": "robot-movement",
    },
    payload: {
      robotId: "robot123",
      toPlanet: {
        id: "planet456",
        movementDifficulty: 0,
      },
      remainingEnergy: 50,
      fromPlanet: {
        id: "1",
        movementDifficulty: 0,
      },
    },
  }

  const mockContext = {
    playerId: "player123",
    playerExchange: "player-exchange",
  }

  const mockPlanet = {
    planetId: "planet456",
    resourceType: "IRON",
  }
  const mockRobot: Robot = {
    id: "1",
    alive: true,
    player: "SwagKingOnTopALWAYS!!",
    planet: mockPlanet,
    maxHealth: 1000,
    maxEnergy: 10,
    energyRegen: 10,
    attackDamage: 10,
    miningSpeed: 10,
    health: 10,
    energy: 10,
    inventory: {
      storageLevel: 0,
      resources: {
        COAL: 0,
        IRON: 0,
        GEM: 0,
        GOLD: 0,
        PLATIN: 0,
      },
      maxStorage: 0,
      usedStorage: 0,
      full: false,
    },
    robotLevels: {
      healthLevel: 0,
      damageLevel: 0,
      miningSpeedLevel: 0,
      miningLevel: 0,
      energyLevel: 0,
      energyRegenLevel: 0,
      storageLevel: 0,
    },
  }

  beforeEach(() => {
    handleRobotMovedInstance = handleRobotMoved()

    logger.info = jest.fn()

    jest.spyOn(relay, "on").mockImplementation((event, callback) => {
      if (event === "RobotMoved") {
        callback(mockEvent, mockContext)
      }
    })
    ;(robotService.getRobot as jest.Mock).mockResolvedValue(mockRobot)
    ;(robotService.updateRobot as jest.Mock).mockResolvedValue(
      Promise.resolve()
    )
    ;(planetService.getPlanet as jest.Mock).mockResolvedValue(mockPlanet)
  })

  it("should not update robot's planet and energy when robot is undefined", async () => {
    ;(robotService.getRobot as jest.Mock).mockResolvedValue(undefined)

    await handleRobotMovedInstance()

    expect(robotService.updateRobot).not.toHaveBeenCalledWith()
  })

  it("should update robot's planet, energy, and log the update", async () => {
    ;(robotService.getRobot as jest.Mock).mockResolvedValue(mockRobot)

    await handleRobotMovedInstance()
    expect(relay.on).toHaveBeenCalledWith("RobotMoved", expect.any(Function))
    expect(robotService.getRobot).toHaveBeenCalledWith("robot123")

    expect(planetService.getPlanet).toHaveBeenCalledWith("planet456")
  })
})
