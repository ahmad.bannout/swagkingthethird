import logger from "../../../utils/logger"
import * as relay from "../../../common/net/relay"
import { updateRobotInventory } from "../../usecase/index"
import {
  GameEvent,
  RobotResourceMined,
  RobotResourceRemoved,
} from "../../../common/types"
import { handleRobotResourceRemoved } from "./mine-robot"

jest.mock("../../../utils/logger")
jest.mock("../../usecase/index")

describe("handleRobotResourceRemoved", () => {
  let handleRobotResourceRemovedInstance: ReturnType<
    typeof handleRobotResourceRemoved
  >

  const mockEvent: GameEvent<RobotResourceRemoved> = {
    headers: {
      type: "RobotResourceRemoved",
      eventId: "event123",
      timestamp: "2024-09-28T12:34:56Z",
      "kafka-topic": "resource-update",
    },
    payload: {
      robotId: "1",
      removedAmount: 3,
      removedResource: "COAL",
      resourceInventory: {
        COAL: 10,
        IRON: 0,
        GEM: 0,
        GOLD: 0,
        PLATIN: 0,
      },
    },
  }

  const mockContext = {
    playerId: "player123",
    playerExchange: "player-exchange",
  }

  beforeEach(() => {
    // Initialize the function that handles the event
    handleRobotResourceRemovedInstance = handleRobotResourceRemoved()

    // Mock logger methods
    logger.info = jest.fn()

    // Spy on relay.on to mock its implementation
    jest.spyOn(relay, "on").mockImplementation((event, callback) => {
      if (event === "RobotResourceRemoved") {
        callback(mockEvent, mockContext)
      }
    })

    // Mock updateRobotInventory function
    ;(updateRobotInventory as jest.Mock).mockResolvedValue(Promise.resolve())
  })

  it("should log resource removal and call updateRobotInventory with correct data", async () => {
    // Call the handleRobotResourceRemovedInstance to simulate the event
    await handleRobotResourceRemovedInstance()

    // Verify that relay.on was set up for the "RobotResourceRemoved" event
    expect(relay.on).toHaveBeenCalledWith(
      "RobotResourceRemoved",
      expect.any(Function)
    )

    // Check that logger.info was called with the correct log message
    expect(logger.info).toHaveBeenCalledWith(
      "3 Resource for Robot: 1 has been removed"
    )

    // Verify that updateRobotInventory was called with the correct payload
    expect(updateRobotInventory).toHaveBeenCalledWith(mockEvent.payload)
  })
})
