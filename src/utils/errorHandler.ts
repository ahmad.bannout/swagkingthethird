import logger from "./logger.js"

export function registerProcessEvents() {
  process.on("unhandledRejection", (err: Error) => {
    logger.error(`Unhandled rejection: ${err.message}`, { stack: err.stack })
  })

  process.on("uncaughtException", (err: Error, origin) => {
    logger.error(`Uncaught exception (origin: ${origin}) err: ${err.message}`, {
      origin,
      stack: err.stack,
    })
  })

  process.on("SIGTERM", (signal) => {
    logger.error(`Received Sigterm: ${signal}`)
  })

  process.on("beforeExit", (code) => {
    logger.warn(`Process will exit with code: ${code}`)
  })
}
