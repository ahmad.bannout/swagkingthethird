import { PlanetDependencies } from "../../common/dependencies/planet-dependency"

export default function makeGetPlanet({ planetRepo }: PlanetDependencies) {
  return async (planetId: string) => {
    const planet = await planetRepo.getPlanet(planetId)
    if (!planet) {
      throw new Error(`Planet with ID ${planetId} not found`)
    }

    return planet
  }
}
