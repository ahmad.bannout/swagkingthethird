import { PlanetDependencies } from "../../common/dependencies/planet-dependency"
import Planet from "../entity/planet"

export default function makeAddPlanet({ planetRepo }: PlanetDependencies) {
  return async (planet: Planet) => {
    try {
      if (!planet.planet) {
        throw new Error("Planet ID is required")
      }
      await planetRepo.savePlanet(planet)
    } catch (error) {
      console.error(`Error adding Planet: ${(error as Error).message}`)
      throw new Error("Failed to add Planet")
    }
  }
}
