import { PlanetDependencies } from "../../common/dependencies/planet-dependency"
import { ResourceDefinition } from "../../common/types"
import logger from "../../utils/logger.js"

export default function makeUpdatePlanet({ planetRepo }: PlanetDependencies) {
  return async (planetId: string, resource: ResourceDefinition) => {
    try {
      const planet = await planetRepo.getPlanet(planetId)
      if (!planet) {
        throw new Error(`Planet with ID ${planetId} not found`)
      }
      planet.resource = resource
      await planetRepo.updatePlanet(planet)
    } catch (error) {
      logger.error("caant update planet resource")
      throw new Error("cant update planet")
    }
  }
}
