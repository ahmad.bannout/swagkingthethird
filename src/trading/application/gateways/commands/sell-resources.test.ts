import { sendCommand } from "../../../../common/net/client"
import { sellResource } from "./sell-resources"

jest.mock("../../../../common/net/client")

describe("sellResource", () => {
  const mockRobotId = "robot123"

  beforeEach(() => {
    ;(sendCommand as jest.Mock).mockResolvedValue(Promise.resolve())
  })

  it("should call sendCommand with the correct parameters", async () => {
    await sellResource(mockRobotId)

    expect(sendCommand).toHaveBeenCalledWith({
      type: "selling",
      data: {
        robotId: mockRobotId,
      },
    })
  })

  it("should return a resolved promise", async () => {
    const result = await sellResource(mockRobotId)

    expect(result).toBeUndefined()
  })
})
