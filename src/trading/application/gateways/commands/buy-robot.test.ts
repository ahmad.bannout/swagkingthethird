import { sendCommand } from "../../../../common/net/client"
import { buyRobots } from "./buy-robot"

jest.mock("../../../../common/net/client")

describe("buyRobots", () => {
  const mockAmount = 5

  beforeEach(() => {
    ;(sendCommand as jest.Mock).mockResolvedValue(Promise.resolve())
  })

  it("should call sendCommand with correct parameters", async () => {
    await buyRobots(mockAmount)

    expect(sendCommand).toHaveBeenCalledWith({
      type: "buying",
      data: {
        robotId: null,
        itemName: "ROBOT",
        itemQuantity: mockAmount,
      },
    })
  })

  it("should return a resolved promise", async () => {
    const result = await buyRobots(mockAmount)

    expect(result).toBeUndefined()
  })
})
