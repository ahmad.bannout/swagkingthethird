import { sendCommand } from "../../../../common/net/client"
import { regenerateRobot } from "./regenerate-robot"

jest.mock("../../../../common/net/client")

describe("regenerateRobot", () => {
  const mockRobotId = "robot123"

  beforeEach(() => {
    ;(sendCommand as jest.Mock).mockResolvedValue(Promise.resolve())
  })

  it("should call sendCommand with the correct parameters", async () => {
    await regenerateRobot(mockRobotId)

    expect(sendCommand).toHaveBeenCalledWith({
      type: "regenerate",
      data: {
        robotId: mockRobotId,
      },
    })
  })

  it("should return a resolved promise", async () => {
    const result = await regenerateRobot(mockRobotId)

    expect(result).toBeUndefined()
  })
})
