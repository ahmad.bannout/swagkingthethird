import { sendCommand } from "../../../../common/net/client"
import { UpgradeType } from "../../../../common/types"
import { buyUpdates } from "./upgrade-robot"

jest.mock("../../../../common/net/client")

describe("buyUpdates", () => {
  const mockRobotId = "robot123"
  const mockUpgradeType: UpgradeType = "DAMAGE_1"
  const mockPlanetId = "planet456"

  beforeEach(() => {
    ;(sendCommand as jest.Mock).mockResolvedValue(Promise.resolve())
  })

  it("should call sendCommand with the correct parameters", async () => {
    await buyUpdates(mockRobotId, mockUpgradeType, mockPlanetId)

    expect(sendCommand).toHaveBeenCalledWith({
      type: "buying",
      data: {
        robotId: mockRobotId,
        planetId: mockPlanetId,
        itemName: mockUpgradeType,
        itemQuantity: 1,
      },
    })
  })

  it("should return a resolved promise", async () => {
    const result = await buyUpdates(mockRobotId, mockUpgradeType, mockPlanetId)

    expect(result).toBeUndefined()
  })
})
