import * as relay from "../../../common/net/relay"
import {
  BankAccountTransactionBookedEvent,
  GameEvent,
} from "../../../common/types"
import logger from "../../../utils/logger"
import bankService from "../../usecase/index"
import { handleUpdateBank } from "./update-bank"

jest.mock("../../../utils/logger")
jest.mock("../../usecase/index")

describe("handleUpdateBank", () => {
  let handleUpdateBankInstance: ReturnType<typeof handleUpdateBank>

  const mockEvent: GameEvent<BankAccountTransactionBookedEvent> = {
    headers: {
      type: "BankAccountTransactionBooked",
      eventId: "",
      timestamp: "",
      "kafka-topic": "",
    },
    payload: {
      balance: 1500,
      playerId: "SwagKing4ever",
      transactionAmount: 100,
    },
  }

  const mockContext = {
    playerId: "",
    playerExchange: "",
  }

  beforeEach(() => {
    handleUpdateBankInstance = handleUpdateBank()

    logger.info = jest.fn()

    jest.spyOn(relay, "on").mockImplementation((event, callback) => {
      if (event === "BankAccountTransactionBooked") {
        callback(mockEvent, mockContext)
      }
    })

    jest
      .spyOn(bankService, "updateBankBalance")
      .mockResolvedValue(Promise.resolve())
  })

  it("should log bank update and call updateBankBalance with the correct payload", async () => {
    await handleUpdateBankInstance()

    expect(relay.on).toHaveBeenCalledWith(
      "BankAccountTransactionBooked",
      expect.any(Function)
    )

    expect(bankService.updateBankBalance).toHaveBeenCalledWith(
      mockEvent.payload.balance
    )

    expect(logger.info).toHaveBeenCalledWith(
      `Bank has been updated, the new Balance is: ${mockEvent.payload.balance}`
    )
  })
})
