import logger from "../../../utils/logger"
import * as relay from "../../../common/net/relay"
import bankService from "../../usecase/index"
import { handleInitializeBank } from "./initialize-bank"
import mapToBank from "../mappers/map-to-bank"
import { GameEvent } from "../../../common/types"
import Bank from "../../entity/bank"

jest.mock("../../../utils/logger")
jest.mock("../../usecase/index")
jest.mock("../mappers/map-to-bank")

describe("handleInitializeBank", () => {
  let handleInitializeBankInstance: ReturnType<typeof handleInitializeBank>

  const mockEvent: GameEvent<any> = {
    headers: {
      type: "BankAccountInitialized",
      eventId: "",
      timestamp: "",
      "kafka-topic": "",
    },
    payload: {
      playerId: "player123",
      balance: 1000,
    },
  }

  const mockContext: relay.RelayContext = {
    playerId: "",
    playerExchange: "",
  }

  const mockBank: Bank = {
    playerId: mockEvent.payload.playerId,
    money: {
      amount: mockEvent.payload.balance,
    },
  }

  beforeEach(() => {
    handleInitializeBankInstance = handleInitializeBank()

    logger.info = jest.fn()
    logger.error = jest.fn()

    jest.spyOn(relay, "on").mockImplementation((event, callback) => {
      if (event === "BankAccountInitialized") {
        callback(mockEvent, mockContext)
      }
    })
    ;(mapToBank as jest.Mock).mockReturnValue(mockBank)
    jest.spyOn(bankService, "addBank").mockResolvedValue(Promise.resolve())
  })

  it("should log bank initialization and call addBank with correct bank", async () => {
    await handleInitializeBankInstance()

    expect(relay.on).toHaveBeenCalledWith(
      "BankAccountInitialized",
      expect.any(Function)
    )

    expect(mapToBank).toHaveBeenCalledWith(mockEvent)

    expect(bankService.addBank).toHaveBeenCalledWith(mockBank)

    expect(logger.info).toHaveBeenCalledWith(
      `Bank has been initialized with balance: ${mockEvent.payload.balance}`
    )
  })
})
