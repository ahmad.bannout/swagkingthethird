import { jest } from "@jest/globals"
import bankDataSource from "../data-access/bank-db"
import makeUpdateBankBalance from "./update-bank"

describe("makeUpdateBankBalance", () => {
  let bankRepo: ReturnType<typeof bankDataSource>
  let updateBankBalance: ReturnType<typeof makeUpdateBankBalance>

  beforeEach(() => {
    bankRepo = bankDataSource()
    updateBankBalance = makeUpdateBankBalance({ bankRepo })

    jest.spyOn(bankRepo, "setBankMoney")
  })

  it("should call setBankMoney with the correct balance if the balance is valid", async () => {
    const validBalance = 1000

    await updateBankBalance(validBalance)

    expect(bankRepo.setBankMoney).toHaveBeenCalledTimes(1)
    expect(bankRepo.setBankMoney).toHaveBeenCalledWith(validBalance)
  })

  it("should throw an error if the balance is null", async () => {
    await expect(updateBankBalance(null as any)).rejects.toThrow(
      "Money is null or smaller than 0"
    )
    expect(bankRepo.setBankMoney).not.toHaveBeenCalled()
  })

  it("should throw an error if the balance is undefined", async () => {
    await expect(updateBankBalance(undefined as any)).rejects.toThrow(
      "Money is null or smaller than 0"
    )
    expect(bankRepo.setBankMoney).not.toHaveBeenCalled()
  })

  it("should throw an error if the balance is less than 0", async () => {
    const invalidBalance = -100

    await expect(updateBankBalance(invalidBalance)).rejects.toThrow(
      "Money is null or smaller than 0"
    )
    expect(bankRepo.setBankMoney).not.toHaveBeenCalled()
  })
})
