import { jest } from "@jest/globals"
import makegetBank from "./get-bank"
import Bank from "../entity/bank"
import bankDataSource from "../data-access/bank-db"

const mockBank: Bank = {
  playerId: "SwagKingOnTopALWAYS!!",
  money: {
    amount: 1000,
  },
}

describe("makegetBank", () => {
  let bankRepo: ReturnType<typeof bankDataSource>
  let getBank: ReturnType<typeof makegetBank>

  beforeEach(() => {
    bankRepo = bankDataSource()
    getBank = makegetBank({ bankRepo })

    jest.spyOn(bankRepo, "getBank") as jest.Mock
  })

  it("should return the correct bank balance if it exists", async () => {
    jest.spyOn(bankRepo, "getBank").mockResolvedValue(mockBank)

    const balance = await getBank()

    expect(bankRepo.getBank).toHaveBeenCalledTimes(1)
    expect(balance).toEqual(mockBank.money.amount)
  })
})
