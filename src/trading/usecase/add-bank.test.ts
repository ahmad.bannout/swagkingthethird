import { jest } from "@jest/globals"
import makeAddBank from "./add-bank"
import Bank from "../entity/bank"
import logger from "../../utils/logger"
import bankDataSource from "../data-access/bank-db"

const mockBank: Bank = {
  playerId: "SwagKingOnTopALWAYS!!",
  money: {
    amount: 1000,
  },
}

describe("makeAddBank", () => {
  let bankRepo: ReturnType<typeof bankDataSource>
  let addBank: ReturnType<typeof makeAddBank>

  beforeEach(() => {
    bankRepo = bankDataSource()
    addBank = makeAddBank({ bankRepo })

    jest.spyOn(logger, "info")
    jest.spyOn(bankRepo, "saveBank")
  })

  it("should log the correct bank balance and call saveBank with the provided bank", async () => {
    await addBank(mockBank)

    expect(logger.info).toHaveBeenCalledTimes(1)
    expect(logger.info).toHaveBeenCalledWith(
      "adding bank with balance: " + mockBank.money.amount
    )

    expect(bankRepo.saveBank).toHaveBeenCalledTimes(1)
    expect(bankRepo.saveBank).toHaveBeenCalledWith(mockBank)
  })
})
